import { Component, OnInit } from '@angular/core';

import { TagService } from '../_services/tag.service';

@Component({
  selector: 'app-edit-tags',
  templateUrl: './edit-tags.component.html',
  styleUrls: ['./edit-tags.component.scss']
})
export class EditTagsComponent implements OnInit {

  tags = [];
  tagsText = '';

  constructor(private tagService: TagService) {
    tagService.getTags().subscribe(tags => {
      this.tags = tags;
      this.tagsText = '';

      // construct string from array of tags
      if (tags) {
        for (const tag of tags) {
          this.tagsText += tag;
          this.tagsText += ", ";
        }
      }
    })
  }

  ngOnInit() {
  }

  // remove tag given its position/index in the array
  delete(index: number) {
    this.tagService.removeTag(index);
  }

  editTags() {
    // replace the existing tags
    this.tagService.addTextAsTags(this.tagsText, true);
  }

}

import { Component, OnInit } from '@angular/core';

import { TagService } from '../_services/tag.service';

@Component({
  selector: 'app-add-tags',
  templateUrl: './add-tags.component.html',
  styleUrls: ['./add-tags.component.scss']
})
export class AddTagsComponent implements OnInit {

  tagsText = '';

  constructor(private tagService: TagService) { }

  ngOnInit() {
  }

  submit() {
    // add tags
    this.tagService.addTextAsTags(this.tagsText);

    // reset text area after adding new tags
    this.tagsText = '';
  }

}
